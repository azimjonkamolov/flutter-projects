﻿import 'package:recipes/utils/class.dart';

class Data {
  static List<Recipe> recipes = [
    Recipe(
        id: '1',
        title: 'Palov',
        imageUrl:
            'https://arbuz.com/wp-content/uploads/2009/07/Uzbek-Plov.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'Clean, wash and julienne carrots',
          'Cut the onion in half circles measuring 1/4 of an inch. Keep the meat in bigger chunks. The reason why I asked to obtain lamb on the bone is to have a nicely flavored bullion. Bullion is ultimately going to give a delightful taste to the rice. If you are purchasing your meat from a butcher shop they always have meat on the bone. If you do not have the bony meat under your discretion, don’t worry about it and proceed with what you have.',
          'Heat the medium cast iron pan or a regular non stick pan. Add the oil and heat it on high heat until you see a slight smoke (do not wait too long).',
          'Using a skimmer carefully and slowly lower the meat in to the oil. You should immediately see the boiling sensation around the meat. Keep on each side for 30 seconds and keep frying until desired color is reached. Remember you are frying the meat on a high heat. Once the meat has a slight brown crust put the onions on top. Add pinch of the cumin half of the salt and the black pepper. Mix everything well and keep mixing to prevent the ingredients from sticking to the bottom of the pan. Since you are cooking on high heat these steps are going to happen quickly. Stay on top of everything. The reason why the main ingredients are cooked on high heat is to have nicely caramelized and tasty base. Be sure not to overdo it. If you think you are more comfortable cooking on lower heat, by all means, do so.',
          'Color of the onions is the essential element do determine the color of the Palov. If you are aiming for a lighter color Palov, there is no need to much caramelize the onions. If you like a darker version, caramelize the onions nicely as shown in the picture below.',
          'Once the onions reach the desired color, put ready carrots in the pan. Add remaining salt and cumin. Stir everything well and fry until carrots are relatively soft. Keep stirring every 30 seconds to prevent the carrots sticking to the bottom of the pan. Once the carrots are ready turn the heat down to medium (5 in electric stove tops) and pour previously prepared 8 cups of boiled water.',
          'Water will start simmering but not boiling. Add the garlic bulb, push it deeper in to the bullion and let everything simmer on medium heat for 1 hour. Meanwhile, depending on the type of the rice you are using, wash it thoroughly. Sometimes it takes multiple washes to get the starch out of the rice.',
          'When the time is up, remove the garlic bulb and evenly distribute the rice with the skimmer. If the water is not enough, add some more water. If using the Basmati rice, keep the water level 1 inch above the rice, for any other time keep it 1/2 inch above the rice level. From this point, until Palov is ready, there is no mixing the ingredients together. You need to keep everything layered until Palov is ready.',
          'Drop the garlic bulb back in the pan. Remember – your heat is on medium. Be attentive, water may evaporate faster than you expect.',
          'Once the rice starts absorbing the water, take out the garlic bulb. Using the skimmer, flip the top layer of the rice to keep it evenly moist (this is the original way of letting the rice absorb the water. If you want to speed up this process close the lid of the pot for about 8 minutes and in medium-high heat let it stand for 10 minutes or until the rice absorbs the water. Check every 2 minutes, because you do not want the bottom of the Palov burnt).',
          'Check the sides of the pan to make sure there is no water remaining. Turn the heat down to medium-low and quickly make a dome from the rice. Put the garlic bulb on top of the rice and push it in.',
          'Cover the rice with a plate suitable for high temperatures. Put the lid on the pan and TURN THE HEAT DOWN TO LOWEST. In electric stoves it is 1. In Gas stoves it is barely visible flame. Let Palov rest for 20 minutes (Basmati 30 minutes).\nIf you are using the faster method I suggested above, 10 minutes are sufficient for the rice to get fully cooked.',
          'Once the time is up, remove the lid and the plate. Remove the garlic bulb, extract the meat and mix the rest of the ingredients using the skimmer.',
          'Bon appetit :)'

        ],
        ingredients: [
          '2 lbs fresh lamb (better if you have some meat on the bone for the taste)',
          '2 medium onions',
          '5 medium carrots',
          '3,5 cups of rice',
          '1 bulb of garlic washed (optional) if the skin is to think remove the top layer only*',
          '1 cup of canola oil',
          '3 tsp of salt',
          '1,5 tsp of ground cumin (or 1 tsp of ground cumin & 1/2 tsp of whole cumin)',
          'pinch of freshly ground black pepper',
          '8 cups of previously boiled water',

        ]),
    Recipe(
        id: '2',
        title: 'Kimbap',
        imageUrl:
            'https://futuredish.com/wp-content/uploads/2018/01/Kimbap.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'Prepare all the ingredients before assembling rolls.',
          'Beat 2 eggs and cook the eggs in a nonstick pan coated with cooking spray.  Cook the eggs until they are gently cooked all the way through.  Cut the eggs in 1/2-inch strips.',
          'Cook the carrots in salted water for 3-4 minutes until firm.',
          'Cook the spinach for 2 minutes in boiling water.  Carefully squeeze the water out of the hot spinach.  Season th spinach with 1/2 tablespoon sesame oil and a pinch of salt.',
          'To assemble the rolls, place a cup of rice on top of a piece of nori.  Spoon the rice edge to edge except for the top edge.Edgeshould be at least 1-2 inches unfilled.',
          'Carefully place the filling in the center of each roll.',
          'Roll the kimbap using a bamboo mat, making sure to push down and pull-back every time you roll a little.',
          'Cut the roll into 1 inch pieces.',
          'Bon appetit :)'
        ],
        ingredients: [
          '4 cups cooked short grain rice',
          '2 eggs',
          '4 long carrot strips cut lengthwise in 1/3-inch x 1/3-inch',
          '4 long pickled Damion radish strips cut lengthwise in 1/3-inch x 1/3-inch',
          '4 long imitation crab meat strips cut lengthwise in 1/3-inch x 1/3-inch',
          '1 cup blanched spinach',
          '1 tablespoon sesame oil',
          'pinch of salt',
          '1 cup chopped Korean style bbq beef',
          '4 nori sheets'


        ]),
    Recipe(
        id: '3',
        title: 'Burrito',
        imageUrl:
            'https://i0.wp.com/mealsheelsandcocktails.com/wp-content/uploads/2017/10/Frenchs_Cheeseburger_Burrito.jpg?fit=1600%2C1067&ssl=1',
        nutrients: [
          Nutrients(name: 'Calories', weight: '100', percent: 0.2),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.7),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.6),
          Nutrients(name: 'Fat', weight: '10gm', percent: 0.3),
        ],
        steps: [
          '2 teaspoons canola oil',
          '1/2 small red onion, diced (1 cup)',
          '1 red bell pepper, seeded and diced',
          '1 cup drained, rinsed canned black beans, preferably low-sodium',
          '1/4 teaspoon chili flakes',
          'Salt and freshly ground black pepper',
          '4 eggs and 4 egg whites',
          '1/3 cup (about 1 1/2 ounce) shredded pepper Jack cheese',
          'Nonstick cooking spray',
          '4 (10 inch) whole wheat tortillas (burrito size)',
          '1/4 cup reduced fat-free sour cream',
          '1/4 cup salsa',
          '1 large tomato, (4 ounces) seeded and diced',
          '1 small avocado (4 ounces), cubed',
          'Hot sauce',
          'Bon appetit :)'

        ],
        ingredients: [
          'Heat the canola oil in a large nonstick skillet over a medium-high heat. Cook the onions and peppers until onions are softened and peppers are slightly charred, about 8 minutes. Add black beans and red pepper flakes and cook until warmed through, another 3 minutes. Season with salt and pepper and transfer to a dish.',
          'Whisk together the eggs and egg whites then stir in the cheese. Spray the skillet with cooking spray, and reheat the skillet over a medium heat. Reduce heat to low and add eggs, scrambling until cooked through, about 3 minutes. Spread each tortilla with 1 tablespoon each sour cream and salsa, then layer with 1/4 of the black bean mixture, 1/4 of the scrambled eggs, some diced tomato and 1/4 of the avocado. Season, to taste, with hot sauce. Roll up burrito-style and serve.'
        ]),
    Recipe(
        id: '4',
        title: 'Norin',
        imageUrl:
            'http://uztravelguide.com/media/zoo/images/naryn_725f4088f0defedbc922fd83c2e3a0f9.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'Boil salted horsemeat or kazy in water and cut into thin strips. Take some part of meat broth and make a stiff dough and let stand for 30-40 minutes',
          'Roll out into 2-3 mm layer, cut into 20x20 squares and boil them in meat broth. Drain them, coat with vegetable oil and stack. After an hour make 3-4 cm wide strips and cut into thin noodles.',
          'Mix noodles and thin strips of meat adding a little black pepper and cumin, and garnish with thinly sliced onions.',
          'Bon appetit :)'
        ],
        ingredients: [
          '1 kg of salted horsemeat or hazy',
          '1 kg of flour',
          '4 onions',
          '1 egg',
          '200 g of vegetable oil',
          'Black pepper and salt to taste',

        ]),
    Recipe(
        id: '5',
        title: 'Borscht Soup',
        imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Borscht_served.jpg/1200px-Borscht_served.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'In a large saucepan, combine the beets, carrots, onion and broth; bring to a boil. Reduce heat; cover and simmer for 30 minutes.',
          'Add tomatoes and cabbage; cover and simmer for 30 minutes or until cabbage is tender. Stir in salt, dill and pepper. Top each serving with sour cream if desired.',
          'Bon appetit :)'
        ],
        ingredients: [
          '2 cups chopped fresh beets',
          '2 cups chopped carrots',
          '2 cups chopped onion',
          '4 cups beef or vegetable broth',
          '1 can (16 ounces) diced tomatoes, undrained',
          '2 cups chopped cabbage',
          '1/2 teaspoon salt',
          '1/2 teaspoon dill weed',
          '1/4 teaspoon pepper',
          'Sour cream, optional',

        ]),
    Recipe(
        id: '6',
        title: 'Pha thai',
        imageUrl:
        'http://static.asiawebdirect.com/m/.imaging/1140x760/website/bangkok/portals/bangkok-com/homepage/magazine/best-pad-thai/pagePropertiesImage.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'Bring a large pot of water to a rolling boil. Add the noodles and cook for 7 to 10 minutes or until tender. Drain the noodles and set aside.',
          'In a large skillet, heat the vegetable oil over medium heat. Add the garlic and cook for 1 to 2 minutes, or until tender.',
          'Whisk the eggs lightly with a fork. Pour them into the skillet and cook just until they solidify, but are still moist, moving the eggs around the skillet slightly as they cook so that they lightly scramble. When the eggs are cooked, remove the skillet from the heat and set aside.',
          'In a small bowl, stir together the soy sauce, lime juice, sugar, fish sauce, and red pepper flakes. Pour the sauce into the skillet with the scrambled eggs. Add the noodles and toss to coat in the sauce.',
          'Sprinkle the green onions, cilantro, and peanuts over the noodles. Toss lightly to combine. Serve warm.',
          'Pad thai noodles have a unique flavor and texture, but if you can\'t find them in your area, try substituting another flat pasta like linguine.',
          'Chef\'s Tip: To get the most juice from your lime, roll it on your countertop while applying pressure before cutting it open. This causes the juice capsules to burst and release more juice.',
          'Bon appetit :)'
        ],
        ingredients: [
          '8 ounces pad thai or lo mein noodles',
          '2 tablespoons vegetable oil',
          '1 clove garlic, minced',
          '2 large eggs',
          '1 1/2 tablespoons soy sauce',
          '2 tablespoons fresh lime juice (from about 1 medium lime)',
          '2 tablespoons brown sugar',
          '1 teaspoon fish sauce',
          '1/8 teaspoon red pepper flakes',
          '3 green onions, sliced',
          '1/4 bunch fresh cilantro, leaves only, roughly chopped',
          '1/4 cup chopped, unsalted peanuts',

        ]),
    Recipe(
        id: '7',
        title: 'Apple pie',
        imageUrl:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Apple_pie.jpg/300px-Apple_pie.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'In a small bowl, combine the sugars, flour and spices; set aside. In a large bowl, toss apples with lemon juice. Add sugar mixture; toss to coat.',
          'Line a 9-in. pie plate with bottom crust; trim even with edge. Fill with apple mixture; dot with butter. Roll remaining crust to fit top of pie; place over filling. Trim, seal and flute edges. Cut slits in crust.',
          'Beat egg white until foamy; brush over crust. Sprinkle with sugar. Cover edges loosely with foil.',
          'Bake at 375° for 25 minutes. Remove foil and bake until crust is golden brown and filling is bubbly, 20-25 minutes longer. Cool on a wire rack.',
          'Bon appetit :)'
        ],
        ingredients: [
          '1/2 cup sugar',
          '1/2 cup packed brown sugar',
          '3 tablespoons all-purpose flour',
          '1 teaspoon ground cinnamon',
          '1/4 teaspoon ground ginger',
          '1/4 teaspoon ground nutmeg',
          '6 to 7 cups thinly sliced peeled tart apples',
          '1 tablespoon lemon juice',
          'Pastry for double-crust pie (9 inches)',
          '1 tablespoon butter',
          '1 large egg white',
          'Additional sugar',

        ]),
    Recipe(
        id: '8',
        title: 'Burger',
        imageUrl:
        'https://images6.alphacoders.com/870/thumb-350-870777.jpg',
        nutrients: [
          Nutrients(name: 'Calories', weight: '200', percent: 0.7),
          Nutrients(name: 'Protein', weight: '10gm', percent: 0.5),
          Nutrients(name: 'Carb', weight: '50gm', percent: 0.9),
        ],
        steps: [
          'Sauce:\n1/2 cup ketchup,\n1/2 cup mayo,\n1 pickle, minced,\n1 tablespoon lemon juice,\n1 teaspoon paprika,\n1 teaspoon chili powder,\n1 tablespoon parsley,\nSalt and pepper, to taste',
          'In a skillet, heat vegetable oil on high and caramelize the onions. Season with salt and pepper; cook until dark in color. Remove cooked onions when finished, set aside. DON\'T clean skillet.',
          'Prepare the sauce. In a medium bowl, combine all sauce ingredients, mix well, and set aside.',
          'Using the same skillet that you cooked the onions in, over high heat add a little oil and place flattened meat patties in skillet. Season with salt and pepper while in the pan.',
          'Cook on each side for 1 minute, flipping twice. While finishing the cook time on the second flip, add 1 slice of American cheese on each patty. Cut heat on skillet and let meat rest in pan until cheese is melted.',
          'Assemble burger in this order: bottom bun, burger sauce, lettuce, caramelized onions, tomato, stacked cheesy patties, top bun.',
          'Bon appetit :)'
        ],
        ingredients: [
          'Vegetable oil',
          '1 Spanish onion, julienned',
          '2 (4 ounces each) ground beef patties, flattened until thin',
          '2 slices American cheese',
          '1 Martin\'s potato hamburger bun',
          '1/2 cup romaine, thinly shredded',
          '1 thick slice of beefsteak tomato',

        ]),
  ];
}
