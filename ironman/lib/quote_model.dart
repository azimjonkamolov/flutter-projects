class Quote
{
  final String quote;
  String personName;
  String imageURL;

  Quote (this.quote, {this.personName, this.imageURL});
}

List<Quote> quotes = [
  new Quote("Only I can change my life. No one can do it for me", personName: "Carol Burnett", imageURL: "assets/images/carol.jpg"),
  new Quote("Doubt is only removed by action. If you’re not working then that’s where doubt comes in", personName: "Conor Mcgregor", imageURL: "assets/images/conor1.jpg"),
  new Quote("We did not come to fear the future. we came here to shape it ", personName: "Barack Obama", imageURL: "assets/images/obama.jpg"),
  new Quote("It's really hard to design products by focus groups. A lot of times, people don't know what they want until you show it to them.", personName: "Steve Jobs", imageURL: "assets/images/steve.jpg"),
  new Quote("Work like hell. I mean you just have to put in 80 to 100 hour weeks every week. [This] improves the odds of success. If other people are putting in 40 hour workweeks and you're putting in 100 hour workweeks, then even if you're doing the same thing, you know that you will achieve in four months what it takes them a year to achieve.", personName: "Elon Musk", imageURL: "assets/images/elon1.jpg"),

];