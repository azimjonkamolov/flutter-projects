import 'package:flutter/material.dart';
import 'package:ironman/home_page.dart';
import 'package:ironman/disney_page.dart';
import 'package:ironman/lennon_page.dart';

void main() =>  runApp(new QuoteApp());

class QuoteApp extends StatelessWidget
{
  @override
  Widget build (BuildContext context)
  {
    return new MaterialApp(
      title: "Motivational Quote App",
      home: new HomePage(),
      routes: <String, WidgetBuilder>
      {
        '/DisneyPage':(BuildContext context) => new DisneyPage(),
        '/LennonPage':(BuildContext context) => new LennonPage()
      },
    );
  }
}