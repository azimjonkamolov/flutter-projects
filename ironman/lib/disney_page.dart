import 'package:flutter/material.dart';
import 'background.dart';
import 'component.dart';
import 'quote_model.dart';

class DisneyPage extends StatelessWidget
{
  final Quote quoteObj = quotes[1];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: new Stack(
          children: <Widget>[
            new ApplyBackground(quoteObj.imageURL),
            new FrostedGlass(quoteObj.quote, personName: quoteObj.personName),

          ],
        )
    );
  }
}