import 'package:flutter/material.dart';
import 'background.dart';
import 'component.dart';
import 'quote_model.dart';

class HomePage extends StatelessWidget
{
  final Quote quoteObj = quotes[0];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: new Stack(
          children: <Widget>[
            new ApplyBackground(quoteObj.imageURL),
            new FrostedGlass(quoteObj.quote, personName: quoteObj.personName),

          ],
        )
    );
  }
}